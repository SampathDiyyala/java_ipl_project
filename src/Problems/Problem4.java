package Problems;

import DeliveriesData.Deliveries;
import DeliveriesData.DelveriesCsvRead;
import MatchesData.CsvFileRead;
import MatchesData.Match;

import java.util.*;

public class Problem4 {
    ListIterator<Deliveries> delit = DelveriesCsvRead.deliverisData.listIterator();
    ListIterator<Match> matchit = CsvFileRead.matchesData.listIterator();
    ArrayList<Integer> matchId2015 = new ArrayList<>();
    ArrayList economyRate = new ArrayList();
    HashMap<String,Double> topEconomyBowlers2015 = new HashMap<>();
    List toptenEconomyBowlers2015List = new ArrayList();

    public  String topEconomialBowlers2015(){
        while (matchit.hasNext()){
            if(matchit.next().getSeason() == 2015){
                matchId2015.add(matchit.next().getMatchId());
            }
        }
        int balls = 0;
        while (delit.hasNext()){

            if(matchId2015.contains(delit.next().getMatchId())){
                int runs = delit.next().getTotalRuns();
                float fr = runs;
                balls = delit.next().getBall();
                int ball = (balls/6);
                float fb = ball;
                double economy =((Math.round((fr/fb)*Math.pow(10,4)))/Math.pow(10,4));
//                System.out.println(fr/fb);
                topEconomyBowlers2015.put(delit.next().getBowler(),economy);
            }

        }
        List<Map.Entry<String, Double>> sortedList = new ArrayList<>(topEconomyBowlers2015.entrySet());
        Collections.sort(sortedList,(o1,o2) -> o1.getValue().compareTo(o2.getValue()));
//        System.out.println(sortedList);
        for(int i=0;i<10;i++){
            toptenEconomyBowlers2015List.add(sortedList.get(i));
        }

//        System.out.println(toptenEconomyBowlers2015List);
        return toptenEconomyBowlers2015List.toString();
//        Iterator it = topEconomyBowlers2015.entrySet().iterator();
    }
}
