package Problems;

import MatchesData.CsvFileRead;
import MatchesData.Match;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;


//Display how many times the team that won the toss and won the match as well for all seasons?
public class Problem5 {

    ListIterator<Match> matchit = CsvFileRead.matchesData.listIterator();
    public static List<HashMap> expected = new ArrayList<>();

    private ArrayList<Integer> seasonList = new java.util.ArrayList<>();
     HashMap<Integer, HashMap<String, Integer>> teamsWonTossAndMatch = new HashMap<>();
//    private  HashMap<String,Integer> teamsWonTossAndMatch = new HashMap<>();
    private  HashMap<Integer,String> teamsWonToss= new HashMap<>();

    public void teamWon() {
        String csvPath = "/home/sampath/JAVA/JAVA_IPL_Project/src/matches.csv";
        String lines = "";
        BufferedReader read = null;
        int skip = 0;
        try {
            read = new BufferedReader(new FileReader(csvPath));
            while ((lines = read.readLine()) != null) {
                if (skip == 0) {
                    skip++;
//                    System.out.println(lines);
                    continue;
                }
//                System.out.println(lines);
                String[] line = lines.split(",");
//                System.out.println(lines);
                this.teamWonToss(line);
            }
            read.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void teamWonToss(String[] data) {
//            System.out.println(data[6]);
        int s1, s2, s3, s4, s5, s6, s7, s8, s9, s10;
        s1 = 2008;
        s2 = 2009;
        s3 = 2010;
        s4 = 2011;
        s5 = 2012;
        s6 = 2013;
        s7 = 2014;
        s8 = 2015;
        s9 = 2016;
        s10 = 2017;
        int arr[] = new int[10];
        int season = Integer.parseInt(data[1]);
        String tossWinner = data[6];
        String matchWinner = data[10];


        try
        {
            if (!(teamsWonTossAndMatch.containsKey(season))){
                if (tossWinner.equals(matchWinner)){
                    teamsWonTossAndMatch.put(season,new HashMap<>(){{put(matchWinner,0);}});
//                    System.out.println(teamsWonTossAndMatch);
//                    System.out.println(tossWinner);
                }
            }
            else if (teamsWonTossAndMatch.containsKey(season) ) {
                if (tossWinner.equals(matchWinner)) {
                    int count = teamsWonTossAndMatch.get(season).get(matchWinner) + 1;
                    teamsWonTossAndMatch.replace(season, new HashMap<>() {{
                        put(matchWinner, count);
                    }});
                }
            }
        }
        catch (Exception e){}

//        Set<Map.Entry<Integer,String>> entrySet = teamsWonTossAndMatch.entrySet();
//        List<Map.Entry<Integer,String>> sortedList = new ArrayList<>(entrySet);
//        Collections.sort(sortedList,(o1,o2) -> o1.getValue().compareTo(o2.getValue()));
//        teamsWonTossAndMatch.entrySet().forEach(entry ->{
//            for(Integer key : teamsWonTossAndMatch.keySet()){
//
//
//            }
//
//        });
        expected.add(teamsWonTossAndMatch);

        System.out.println(expected);

    }
}