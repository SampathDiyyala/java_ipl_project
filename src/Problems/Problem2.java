package Problems;

import MatchesData.CsvFileRead;
import MatchesData.Match;

import java.util.*;

public class Problem2 {
    private ArrayList<Integer> seasonList = new ArrayList<>();
    private ArrayList list = new ArrayList();
    private HashMap<Integer, Object> result = new HashMap<>();
    private HashMap<String, Integer> winnerTeam2008 = new HashMap<>();
    private HashMap<String, Integer> winnerTeam2009 = new HashMap<>();
    private HashMap<String, Integer> winnerTeam2010 = new HashMap<>();
    private HashMap<String, Integer> winnerTeam2011 = new HashMap<>();
    private HashMap<String, Integer> winnerTeam2012 = new HashMap<>();
    private HashMap<String, Integer> winnerTeam2013 = new HashMap<>();
    private HashMap<String, Integer> winnerTeam2014 = new HashMap<>();
    private HashMap<String, Integer> winnerTeam2015 = new HashMap<>();
    private HashMap<String, Integer> winnerTeam2016 = new HashMap<>();
    private HashMap<String, Integer> winnerTeam2017 = new HashMap<>();
    private HashSet teams = new HashSet();

    public String matchesWonPerYear()
    {
        ListIterator<Match> it = CsvFileRead.matchesData.listIterator();

        Seasons seasons = new Seasons();
        seasonList = seasons.getSeasons();
        int arr[] = {2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017};
        int count[] = new int[10];
        int c1 = 0, c2 = 0;


        while (it.hasNext())
        {
//            System.out.println(it.next().getTeam1());
            teams.add(it.next().getTeam1());
            try {
                if (it.next().getSeason() == arr[0]) {
                    winnerTeam2008.put(it.next().getWinner(), ++count[0]);
                }
                else if (it.next().getSeason() == arr[1]) {
                    winnerTeam2009.put(it.next().getWinner(), ++count[1]);
                }
                else if (it.next().getSeason() == arr[2]) {
                    winnerTeam2010.put(it.next().getWinner(), ++count[2]);
                }
                else if (it.next().getSeason() == arr[3]) {
                    winnerTeam2011.put(it.next().getWinner(), ++count[3]);
                }
                else if (it.next().getSeason() == arr[4]) {
                    winnerTeam2012.put(it.next().getWinner(), ++count[4]);
                }
                else if (it.next().getSeason() == arr[5]) {
                    winnerTeam2013.put(it.next().getWinner(), ++count[5]);
                }
                else if (it.next().getSeason() == arr[6]) {
                    winnerTeam2014.put(it.next().getWinner(), ++count[6]);
                }
                else if (it.next().getSeason() == arr[7]) {
                    winnerTeam2015.put(it.next().getWinner(), ++count[7]);
                }
                else if (it.next().getSeason() == arr[8]) {
                    winnerTeam2016.put(it.next().getWinner(), ++count[8]);
//                    System.out.println(it.next().getWinner());
                }
                else if (it.next().getSeason() == arr[9]) {
                    winnerTeam2017.put(it.next().getWinner(), ++count[9]);
                }
            }
            catch (NoSuchElementException e)
            {

            }

//        System.out.println(teams);



        }
//        System.out.println(winnerTeam2010);
        try{Thread.sleep(500);}catch (Exception e){};
        for(int season : arr){
            if(season==2008)
            {
                result.put(season,winnerTeam2008);
            }
            else if (season==2009)
            {
                result.put(season,winnerTeam2009);
            }
            else if (season==2009)
            {
                result.put(season,winnerTeam2009);
            }
            else if (season==2010)
            {
                result.put(season,winnerTeam2010);
            }
            else if (season==2011)
            {
                result.put(season,winnerTeam2011);
            }
            else if (season==2012)
            {
                result.put(season,winnerTeam2012);
            }
            else if (season==2013)
            {
                result.put(season,winnerTeam2013);
            }
            else if (season==2014)
            {
                result.put(season,winnerTeam2014);
            }
            else if (season==2015)
            {
                result.put(season,winnerTeam2015);
            }
            else if (season==2016)
            {
                result.put(season,winnerTeam2016);
            }
            else if (season==2017)
            {
                result.put(season,winnerTeam2017);
            }
        }
//        System.out.println(teams);
//        System.out.println(winnerTeam2016);
//        System.out.println(result.toString());
//        for(int key : result.keySet()){
//            System.out.println(key + " : " + result.get(key));
//        }
//        Object o = result;
        List<Map.Entry<Integer,Object>> sortedList = new ArrayList<>(result.entrySet());
        Collections.sort(sortedList, Comparator.comparing(Map.Entry::getKey));
//        System.out.println(sortedList);
//        System.out.println(winnerTeam2008);
        String key  = "Mumbai Indians";
//        System.out.println(winnerTeam2008.get(key));
        return winnerTeam2008.get(key).toString();
    }
}
