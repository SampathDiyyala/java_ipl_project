package Problems;

import DeliveriesData.DelveriesCsvRead;
import MatchesData.CsvFileRead;
import org.junit.Test;

import static org.testng.AssertJUnit.assertEquals;


public class Problem4Test {
    @Test
    public void testProblem4(){
        CsvFileRead ob= new CsvFileRead();
        ob.readData();
        DelveriesCsvRead del = new DelveriesCsvRead();
        del.readDelData();
        var pro4 = new Problem4();
        String str ="[HV Patel=0.0, M Ashwin=0.0, TG Southee=0.0, R Bhatia=0.0, GS Sandhu=0.0, BE Hendricks=0.0, SL Malinga=0.0, AR Patel=0.0, J Yadav=0.0, P Negi=0.0]";
        assertEquals(str,pro4.topEconomialBowlers2015());

    }
}