package Problems;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import static org.testng.Assert.*;

//import static org.junit.Assert.assertNotNull;

public class Problem5Test {
    @Test
    public void testProblem5() throws IOException {
        var data = Problem5.expected;
        var pro5 = new Problem5();
        String csvPath = "/home/sampath/JAVA/JAVA_IPL_Project/src/matches.csv";
        String lines = "";
        BufferedReader read = null;
        int skip = 0;
        read = new BufferedReader(new FileReader(csvPath));
        while ((lines = read.readLine()) != null) {
            if (skip == 0) {
                skip++;
//                    System.out.println(lines);
                continue;
            }
//                System.out.println(lines);
            String[] line = lines.split(",");
            assertEquals(data,pro5.teamWonToss(line));
            assertNotNull(pro5.teamWonToss(line));
            assertNull(null);

        }

    }
}
