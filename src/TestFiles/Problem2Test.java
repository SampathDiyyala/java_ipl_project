package Problems;

import MatchesData.CsvFileRead;
import org.junit.Test;

import static org.testng.AssertJUnit.assertEquals;

public class Problem2Test {
    @Test
    public void testProblem2(){
        Problem2 obj2 = new Problem2();
        CsvFileRead ob= new CsvFileRead();
        ob.readData();
        String str = "[2008={Mumbai Indians=36, Kings XI Punjab=32, Deccan Chargers=25, Delhi Daredevils=33, Rajasthan Royals=35, Royal Challengers Bangalore=17, Kolkata Knight Riders=34, Chennai Super Kings=31}, 2009={Mumbai Indians=19, Kings XI Punjab=20, Deccan Chargers=28, Delhi Daredevils=27, Rajasthan Royals=16, Royal Challengers Bangalore=18, Kolkata Knight Riders=26, Chennai Super Kings=25}, 2010={Mumbai Indians=21, Kings XI Punjab=9, Deccan Chargers=23, Rajasthan Royals=5, Delhi Daredevils=22, Kolkata Knight Riders=18, Royal Challengers Bangalore=14, Chennai Super Kings=24}, 2011={Mumbai Indians=20, Pune Warriors=1, Kochi Tuskers Kerala=22, Kings XI Punjab=10, Deccan Chargers=23, Delhi Daredevils=6, Rajasthan Royals=11, Kolkata Knight Riders=18, Royal Challengers Bangalore=24, Chennai Super Kings=19}, 2012={Mumbai Indians=18, Deccan Chargers=21, Rajasthan Royals=7, Delhi Daredevils=20, Royal Challengers Bangalore=19, Kolkata Knight Riders=11, Chennai Super Kings=15}, 2013={Mumbai Indians=19, Sunrisers Hyderabad=17, Kings XI Punjab=18, Rajasthan Royals=14, Delhi Daredevils=15, Royal Challengers Bangalore=16, Chennai Super Kings=9}, 2014={Mumbai Indians=12, Kings XI Punjab=2, Delhi Daredevils=1, Rajasthan Royals=11, Royal Challengers Bangalore=5, Kolkata Knight Riders=7, Chennai Super Kings=13}, 2015={Mumbai Indians=5, Sunrisers Hyderabad=2, Rajasthan Royals=12, Rising Pune Supergiants=6, Kolkata Knight Riders=11, Royal Challengers Bangalore=10, Chennai Super Kings=8}, 2016={Gujarat Lions=9, Mumbai Indians=6, Sunrisers Hyderabad=11, Rising Pune Supergiants=10, Kolkata Knight Riders=4, Royal Challengers Bangalore=7}, 2017={Mumbai Indians=9, Rising Pune Supergiant=10, Sunrisers Hyderabad=8, Kings XI Punjab=6, Kolkata Knight Riders=5, Royal Challengers Bangalore=7}]";

       assertEquals(obj2.matchesWonPerYear().length(),str.length());
    }

}