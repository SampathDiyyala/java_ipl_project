package Problems;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        Problem1Test.class,Problem2Test.class,Problem3Test.class,Problem4Test.class
})

public class JunitSuite {
}
