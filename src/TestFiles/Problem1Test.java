package Problems;

import MatchesData.CsvFileRead;
import org.junit.Test;

import static org.testng.AssertJUnit.assertEquals;

public class Problem1Test {

    @Test
    public void testProblem1(){
        var obj = new Problem1();
        CsvFileRead ob= new CsvFileRead();
        ob.readData();

        String str = "[2009=57, 2008=58, 2017=59, 2015=59, 2016=60, 2010=60, 2014=60, 2011=73, 2012=74, 2013=76]";
        assertEquals(str,obj.matchesPerYear());
//        assertSame(str,obj.matchesPerYear());
//        assertTrue(str==obj.matchesPerYear());
    }

//    public static void main(String[] args){
//        CsvFileRead ob= new CsvFileRead();
//        ob.readData();
//        DelveriesCsvRead del = new DelveriesCsvRead();
//        del.readDelData();
//        Problem1 obj = new Problem1();
//        System.out.println(obj.matchesPerYear());
//    }
}